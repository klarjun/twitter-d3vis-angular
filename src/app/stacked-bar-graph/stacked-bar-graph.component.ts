import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3';


@Component({
  selector: 'app-stacked-bar-graph',
  templateUrl: './stacked-bar-graph.component.html',
  styleUrls: ['./stacked-bar-graph.component.scss']
})
export class StackedBarGraphComponent implements OnInit {

  constructor() { }

  dropDownItems = ['2017-08-26', '2017-08-27', '2017-08-28',
    '2017-08-29', '2017-08-30', '2017-08-31', '2017-09-01', '2017-09-02', '2017-09-03', '2017-09-04', '2017-09-05'];

  selectedDate: string;
  assetsPrefix = '../../assets/data';

  ngOnInit() {

    d3.json(`${this.assetsPrefix}/TweetsData.json`, (err: any, graph: any) => {
      console.log(graph);
      this.loadGraph(graph);
    });

  }

  loadGraph(dataset: any) {
    const w = 900;                        // width
    const h = 700;                        // height
    const padding = { top: 40, right: 40, bottom: 40, left: 40 };
    // Set up stack method
    const stack = d3.layout.stack();

    stack(dataset);

    const color_hash = {
        0 : ['Believes_legitimate: True', 'blue'],
        1 : ['Believes_legitimate: False', 'red'],
        2 : ['Neutral', '#2ca02c']

    };


    // Set up scales
    const xScale = d3.time.scale()
      .domain([new Date(dataset[0][0].time), d3.time.day.offset(new Date(dataset[0][dataset[0].length - 1].time), 1)])
      .rangeRound([0, w - padding.left - padding.right]);

    const yScale = d3.scale.linear()
      .domain([0,
        d3.max(dataset, (d: any) => {
          return d3.max(d, (d: any) => {
            return d.y0 + d.y;
          });
        })
      ])
      .range([h - padding.bottom - padding.top, 0]);

    const xAxis = d3.svg.axis()
             .scale(xScale)
             .orient('bottom')
                         .ticks(d3.time.days, 1);



    const yAxis = d3.svg.axis()
             .scale(yScale)
             .orient('left')
             .ticks(10);



    // Easy colors accessible via a 10-step ordinal scale
    const colors = d3.scale.category10();

    // Create SVG element
    const svg = d3.select('#mbars')
          .append('svg')
          .attr('width', w)
          .attr('height', h);

    // Add a group for each row of data
    let rect: any;
    let groups: any = svg.selectAll('g')
      .data(dataset)
      .enter()
      .append('g')
      .attr('class', 'rgroups')
      .attr('transform', 'translate(' + (padding.left + 1) + ',' + (h - padding.bottom) + ')')
      .style('fill', (d, i) => {
        return color_hash[dataset.indexOf(d)][1];
      });

    // Add a rect for each data value
    const rects = groups.selectAll('rect')
      .data((d) => d)
      .enter()
      .append('rect')
      .attr('width', 40)
      .style('fill-opacity', 1e-6);


    rects.transition()
         .duration(1000)
         .ease('linear')
        .attr('x', (d: any) => {
        return xScale(new Date(d.time));
      })
      .attr('y', (d: any) => {
        return -(- yScale(d.y0) - yScale(d.y) + (h - padding.top - padding.bottom) * 2);
      })
      .attr('height', (d: any) => {
        return -yScale(d.y) + (h - padding.top - padding.bottom);
      })
      .attr('width', 35)
      .style('fill-opacity', 1);


    svg.append('g')
        .attr('class', 'x axis')
        .attr('transform', 'translate(40,' + (h - padding.bottom) + ')')
        .call(xAxis);


    svg.append('g')
        .attr('class', 'y axis')
        .attr('transform', 'translate(' + padding.left + ',' + padding.top + ')')
        .call(yAxis);

      // adding legend

    const legend = svg.append('g')
              .attr('class', 'legend')
              .attr('x', w - padding.right - 65)
              .attr('y', 25)
              .attr('height', 100)
              .attr('width', 100);

    legend.selectAll('g').data(dataset)
          .enter()
          .append('g')
          .each(function(d, i) {
            const g = d3.select(this);
            g.append('rect')
              .attr('x', w - padding.right - 100)
              .attr('y', i * 25 + 10)
              .attr('width', 10)
              .attr('height', 10)
              .attr('margin', 20)
              .style('fill', color_hash[String(i)][1]);

            g.append('text')
             .attr('x', w - padding.right - 80)
             .attr('y', i * 25 + 20)
             .attr('height', 30)
             .attr('width', 100)
                         .style('fill', color_hash[String(i)][1])
                         .style('size', 10)
             .text(color_hash[String(i)][0]);
          });

    svg.append('text')
      .attr('transform', 'rotate(-90)')
      .attr('y', 0 - 5)
      .attr('x', 0 - (h / 2))
      .attr('dy', '1em')
      .text('Number of Messages');

    svg.append('text')
       .attr('class', 'xtext')
       .attr('x', w / 2 - padding.left)
       .attr('y', h - 5)
       .attr('text-anchor', 'middle')
       .text('Days');

    svg.append('text')
        .attr('class', 'title')
        .attr('x', (w / 2))
        .attr('y', 20)
        .attr('text-anchor', 'middle')
        .style('font-size', '16px')
        .style('text-decoration', 'underline')
        .text('Number of messages per day.');

    // On click, update with new data

    const optionClicked = (event) => {

      if (event.target.className !== 'dropdown-item') {
        return;
      }

      const date = this.selectedDate;
      let str: string;
      if (date == '2017-08-26') {
          str = '26.json';
        } else if (date == '2017-08-27') {
          str = '27.json';
        } else if (date == '2017-08-28') {
          str = '28.json';
        } else if (date == '2017-08-29') {
          str = '29.json';
        } else if (date == '2017-08-30') {
          str = '30.json';
        } else if (date == '2017-08-31') {
          str = '31.json';
        } else if (date == '2017-09-01') {
          str = '01.json';
        } else if (date == '2017-09-02') {
          str = '02.json';
                  } else if (date == '2017-09-03') {
          str = '03.json';
        } else if (date == '2017-09-04') {
          str = '04.json';
        } else {
          str = '05.json';
        }

      d3.json(`${this.assetsPrefix}/${str}`, (json) => {

          dataset = json;
          stack(dataset);

          xScale.domain([new Date(0, 0, 0, dataset[0][0].time, 0, 0, 0), new Date(0, 0, 0, dataset[0][dataset[0].length - 1].time, 0, 0, 0)])
          .rangeRound([0, w - padding.left - padding.right]);

          yScale.domain([0,
                  d3.max(dataset, (d: any) => {
                    return d3.max(d, (d: any) => {
                      return d.y0 + d.y;
                    });
                  })
                ])
                .range([h - padding.bottom - padding.top, 0]);

          xAxis.scale(xScale)
               .ticks(d3.time.hour, 2)
               .tickFormat(d3.time.format('%H'));

          yAxis.scale(yScale)
               .orient('left')
               .ticks(10);

          groups = svg.selectAll('.rgroups')
                      .data(dataset);

          groups.enter().append('g')
                      .attr('class', 'rgroups')
                      .attr('transform', 'translate(' + padding.left + ',' + (h - padding.bottom) + ')')
                      .style('fill', (d, i) => {
                          return colors(i);
                      });


          rect = groups.selectAll('rect')
                      .data((d) => d);

          rect.enter()
                        .append('rect')
                        .attr('x', w)
                        .attr('width', 1)
                        .style('fill-opacity', 1e-6);

          rect.transition()
                      .duration(1000)
                      .ease('linear')
                      .attr('x', (d) => {
                          return xScale(new Date(0, 0, 0, d.time, 0, 0, 0));
                      })
                      .attr('y', (d) => {
                          return -(- yScale(d.y0) - yScale(d.y) + (h - padding.top - padding.bottom) * 2);
                      })
                      .attr('height', (d) => {
                          return -yScale(d.y) + (h - padding.top - padding.bottom);
                      })
                      .attr('width', 15)
                      .style('fill-opacity', 1);

          rect.exit()
               .transition()
               .duration(1000)
               .ease('circle')
               .attr('x', w)
               .remove();

          groups.exit()
               .transition()
               .duration(1000)
               .ease('circle')
               .attr('x', w)
               .remove();


          svg.select('.x.axis')
             .transition()
             .duration(1000)
             .ease('circle')
             .call(xAxis);

          svg.select('.y.axis')
             .transition()
             .duration(1000)
             .ease('circle')
             .call(yAxis);

          svg.select('.xtext')
             .text('Hours');

          svg.select('.title')
              .text('Number of messages per hour on ' + date + '.');
        });


    };

    document.addEventListener('click', optionClicked);


  }

}
